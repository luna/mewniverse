mewniverse
==========

basically a curated list of Blob Cat emoji
and Meowmoji for use in the fediverse.

There is a clear bias towards more Meowmoji
in this list, because *meow*.

This repository has utilities on:
 - downloading the list
 - compressing static emojis above 50KB
 - generating APNG versions out of GIF emoji

## Notes

 - Not all APNG versions will be smaller than their GIF
    versions. Proceed with caution.
 - The script will directly download the emojis from their
    source, which is Discord.

## Installation

Requirements:
 - Python 3.6+
 - (optional) ImageMagick if you want to compress images
 - (optional) FFmpeg to generate APNG emoji

```
git clone git@gitlab.com:luna/mewniverse.git
cd mewniverse

python3 -m pip install --user --editable .

# or, if root perms
python3 -m pip install --editable .

# after that, edit the aniprefix.txt file
# with the file prefix you want for your animated
# emoji (all emojis with category ending in _anim)
```

## Attribution

Before explaining usage, I must tell the licensing of the
assets you can download with mewniverse.

The code was written by me and the licsensing is on `./LICENSE`

Blob Cat Emoji and Meowmoji were made by Nitro Blob Hub,
they are copyrighted and licensed under Apache 2.0.

Nitro Blob Hub can be found at `discord[dot]gg/yATKMY8`.

Apache 2.0: https://github.com/googlei18n/noto-emoji/blob/master/LICENSE
A copy of the Apache 2.0 License is in this repository under the `EMOJI_LICENSE` file.

## Usage

```bash
# stay on mewniverse folder, as most stuff will be stored there

# download all emoji specified in the emoji.csv file
meow download

# compress static emoji that are above 50kb
meow filter_static

# generate apng versions of animated emoji
meow gen_apng

# you can compress the emojis folder as you wish,
# apng versions of animated emoji(any category
# ending with '_anim') will be in the *_anim/apng folder
```
