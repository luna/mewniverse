"""
meow.py - main file for mewniverse
"""
import csv
import subprocess
from collections import namedtuple, defaultdict
from pathlib import Path

import click
import requests


# main emoji namedtuple so we don't need
# to mess with row[0], row[1] and row[2]
Emoji = namedtuple('Emoji', 'type name url')

EMOJI_FOLDER = Path.cwd() / Path('emojis')


def err(message: str):
    """Print a message in a red foreground."""
    click.secho(message, fg='red')


def _to_kb(bytecount: int) -> int:
    return round(bytecount / 1024)


def read_emoji(reader: csv.reader) -> tuple:
    """Get emoji information from a CSV reader.

    Parameters
    ----------
    reader: csv.reader
        Reader containing emoji information,
        yielding rows containing the type,
        the name, and the URL of the emoji.

    Returns
    -------
    tuple:
        A list of Emoji instances and
        a dictionary mapping "emoji ids" to
        Emoji instances.
    """
    mapping = defaultdict(dict)
    emoji_list = []

    for row in reader:
        # ignore blank lines
        if not row:
            continue

        emoji = Emoji(*row)

        # since we're dealing with emojis coming
        # from discord, we can use the emoji id
        # that is embedded in the url for
        # our mapping.

        # also the current way to fetch the emoji
        # id is really ugly.
        emoji_id = emoji.url.split('/')[-1].split('.')[0]

        mapping[emoji.type][emoji_id] = emoji
        emoji_list.append(emoji)

    return emoji_list, mapping


@click.group()
@click.pass_context
def cli(ctx):
    """Dab!"""
    with open('./emoji.csv', 'r') as emoji_file:
        csv_reader = csv.reader(emoji_file, delimiter=',')

        # all_emoji handles a list of Emoji
        # emojis handles a dictionary from emoji_id(any) to Emoji
        ctx.all_emoji, ctx.emojis = read_emoji(csv_reader)

    # this is probably a bad way of fetching the file but
    # i'm doing it.
    anim_prefix = (Path.cwd() / Path('aniprefix.txt')).read_text().strip()
    ctx.anim_prefix = anim_prefix


@cli.command()
def meow():
    """meow at you"""
    click.secho('meow!', fg='yellow')


@cli.command()
@click.pass_context
def download(ctx):
    """Fetch all emoji in the emoji.csv file"""
    root = ctx.parent
    all_emoji = root.all_emoji

    success = 0

    for idx, emoji in enumerate(all_emoji):
        click.secho(f'[{idx + 1}/{len(all_emoji)}] '
                    f'Downloading {emoji.name} ({emoji.type})')

        # NOTE: ugly way to extract extension.
        extension = emoji.url.split('/')[-1].split('.')[1]
        emoji_filename = f'{emoji.name}.{extension}'

        if emoji.type.endswith('_anim'):
            emoji_filename = f'{root.anim_prefix}{emoji_filename}'

        emoji_folder = EMOJI_FOLDER / f'{emoji.type}'
        emoji_folder.mkdir(exist_ok=True)

        emoji_path = emoji_folder / emoji_filename

        # first check if we have it downloaded
        if emoji_path.exists():
            click.secho(f'{emoji.name} already downloaded!')
            success += 1
            continue

        resp = requests.get(emoji.url)

        # if requesting failed we should skip to the next one.
        if resp.status_code != 200:
            err('Failure downloading {emoji.name}: status code not 200')
            continue

        # write to emoji file
        with emoji_path.open('wb') as emoji_file:

            # chunk writes to not use tons of memory
            for chunk in resp.iter_content(chunk_size=512):
                emoji_file.write(chunk)

        success += 1

    click.secho('Successfully downloaded '
                f'{success} out of {len(all_emoji)} emoji',
                fg='green')


@cli.command()
@click.pass_context
def filter_static(ctx):
    """Pass through all static emojis and optimize
    their size if needed.

    This process is not guaranteed to work.

    Only emojis below 50KB will be modified.
    """
    root = ctx.parent

    # work only on non-animated emoji (also not APNG)
    for cat in root.emojis:

        # skip _anim categories
        if cat.endswith('_anim'):
            continue

        out = subprocess.check_output(f'find emojis/{cat} -type f -size +50k',
                                      shell=True)

        out = out.decode()

        if not out:
            print(f'{cat}: no emoji found to work through')
            continue

        lines = out.strip().split('\n')
        for emoji_path_raw in lines:
            emoji_path = Path(emoji_path_raw)

            pre_stat = emoji_path.stat()

            # use mogrify to optimize the size of the image
            # in-place
            subprocess.check_output(
                f'mogrify -format png -quality 50 {emoji_path_raw}',
                shell=True)

            new_stat = emoji_path.stat()

            old_size = _to_kb(pre_stat.st_size)
            new_size = _to_kb(new_stat.st_size)

            print(f'optimized {emoji_path_raw} ({old_size}kb => {new_size}kb)')


@cli.command()
@click.pass_context
def gen_apng(ctx):
    """Generate APNG versions for animated emoji."""
    root = ctx.parent

    success = 0

    for cat in root.emojis:
        # ignore non-animated categories
        if not cat.endswith('_anim'):
            continue

        folder_path = EMOJI_FOLDER / cat

        apng_folder_path = folder_path / 'apng'
        apng_folder_path.mkdir(exist_ok=True)

        for child in folder_path.iterdir():
            # skip all non-files (such as our own
            # apng directory)
            if not child.is_file():
                continue

            apng_filename = child.name.replace('.gif', '.png')
            apng_path = apng_folder_path / apng_filename

            subprocess.check_output(
                f'ffmpeg -i {child} -f apng -plays 0 {apng_path}',
                shell=True
            )

            success += 1

    print(f'Generated {success} apng emojis')
