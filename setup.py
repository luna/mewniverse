from setuptools import setup

setup(
    name='mewniverse',
    version='0.1',
    install_requires=[
        'Click==6.7',
        'requests==2.19.1',
    ],
    entry_points='''
    [console_scripts]
    meow=meow:cli
    '''
)
